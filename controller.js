'use strict';
class App {
    constructor() {
        this.backUrl = 'http://localhost:8080';
        this.cl = $(".authrow:first").clone();
        this.clickHandler();

    }

    send(url, type, body, form) {
        let that = this;
        return new Promise(function (resolve, reject) {
        let xmlHttp = new XMLHttpRequest();

        xmlHttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                resolve(that.dataParse(xmlHttp.responseText) );
            } else if (xmlHttp.readyState === 4 && xmlHttp.status === 405) {
                console.log(that.dataParse(xmlHttp.responseText) );
                alert( that.dataParse(xmlHttp.responseText).error);
            }
        };
        xmlHttp.open(type, that.backUrl + url, true);
        if (form) {
            xmlHttp.send(body);
        }
         else  {
            xmlHttp.setRequestHeader("Content-type", "application/json");
            xmlHttp.send(JSON.stringify(body));
        }


        });
    }




    clickHandler() {
        let that = this;

        $("#addbook").click(async function (event) {
            event.preventDefault();
            let photo = $('#photo').prop('files');
            let formData = new FormData();
            formData.append('file', photo[0]);

            let authors = [];
            $('.authrow').each(function () {

                authors.push({
                    firstName: $(this).find('.firstName').val(),
                    lastName: $(this).find('.lastName').val()
                })
            });
            let body = {
                book: $("#book").val(),
                authors: authors,
                photo:''
            };

            let data;
            if ( photo.length !== 0)  {
                data = await that.send('/book/photo', 'POST', formData, true);
                body.photo = data.Data;
            }
            that.send('/book/add', 'POST', body).then(
                response => {
                    alert('Success');
                    $(':input').val('');
                })
        });


        $("#addauthor").click(function (event) {
            event.preventDefault();
            that.cl .clone().insertAfter( "#add" );
        });

        $("#upploadBook").click(function (event) {
            event.preventDefault();
            $(".show").hide();
            $(".uppload").show();
            let valueFist ='';
            let valuelast ='';
            that.send('/book/authors', 'GET').then(
                response => {
                    response.authors.forEach(function (val) {
                        valueFist = valueFist + '<option value="' + val.firstName + '"></option>';
                        valuelast = valuelast + '<option value="' + val.lastName + '"></option>';

                    });
                    $('#drop-firstName').html(valueFist);
                    $('#drop-lastName').html(valuelast);
                })
        });

        $("#showBook").click(function (event) {
            event.preventDefault();
            $(".uppload").hide();
            $(".show").show();
            let table;
            that.send('/book/free', 'GET').then(
                response => {
                    response.Books = response.Books.reverse();
                    response.Books.forEach(function (book) {
                        let cover = '';
                        let firstTd = '<td rowspan="'+  book.Authors.length  + '">';
                        if (book.photo) cover =  '<img src="' + that.backUrl +'/book/photo/' + book.photo +' " /></td>';
                        if (book.Authors.length === 0) {
                            firstTd = '<td>';
                        };
                        cover = firstTd + cover ;
                        table = table + '<tr>' + firstTd + book.name + '</td>';

                        if (book.Authors.length !== 0) {
                            book.Authors.forEach(function (author, i) {
                                if (i === 0 ) {
                                    table = table + '<td>' + author.firstName + '</td><td>' + author.lastName + '</td>' + cover  + '</tr>';
                                } else {
                                    table = table + '<tr><td>' + author.firstName + '</td><td>' + author.lastName + '</td></tr>';
                                }


                            });
                        } else {
                            table = table  + '<td></td><td></td>' + cover  + '</tr>';
                        }
                    });

                    $('#apnd').html(table)
                })

        })
    }



    dataParse(data) {
        return JSON.parse(data);
    }



}




let app1 = new App();
